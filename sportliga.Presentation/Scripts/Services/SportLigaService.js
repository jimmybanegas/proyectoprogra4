﻿'use strict';
angular.module('app.services')
    .factory('SportLiga', function ($http, Server, $cookieStore) {
        return {
            //Sección de las ligas
            addLeague: function (addLeagueModel, success, error) {
                $http
                    .post(
                       Server.get() + '/addleague', addLeagueModel)
                    .success(function (response) {
                        success(response);
                    })
                    .error(error);
            },

            getAvailableLeagues: function (success, error) {
                $http
                    .get(
                        Server.get() + '/leagues/available', {
                            headers: { 'Authorization': $cookieStore.get('access_token') }
                        })
                    .success(function (response) {
                        success(response);
                    })
                    .error(error);
            },

            getSuscribedLeagues: function (success, error) {
                $http.get(Server.get() + '/leagues/suscribed', {
                    headers: { 'Authorization': $cookieStore.get('access_token') }

                })
                    .success(function (response) {
                        success(response);
                    }).error(error);
            },

            //Sección de los equipos
            addTeam: function (addTeamModel, success, error) {
                $http
                    .post(
                         Server.get() + '/addTeam', addTeamModel)
                    .success(function (response) {
                        success(response);
                    })
                    .error(error);
            },

            getAvailableTeams: function (success, error) {
                $http
                    .get(
                        Server.get() + '/teams/available', {
                            headers: { 'Authorization': $cookieStore.get('access_token') }
                        })
                    .success(function (response) {
                        success(response);
                    })
                    .error(error);
            },

            //Sección de los partidos
            addMatch: function (addMatchModel, success, error) {
                $http
                    .post(
                         Server.get() + '/addMatch', addMatchModel)
                    .success(function (response) {
                        success(response);
                    })
                    .error(error);
            },

            //Sección de las predicciones
            addPrediction: function (addPredictionModel, success, error) {
                $http
                    .post(
                         Server.get() + '/addPrediction', addPredictionModel)
                    .success(function (response) {
                        success(response);
                    })
                    .error(error);
            }
        };
    });