﻿namespace PrediLiga.Domain.Entities
{
    public class Prediction : IEntity
    {
        public virtual long Usuario { get; set; }

       
        public virtual int Equipo1 { get; set; }

        public virtual int Equipo2 { get; set; }

        public virtual long Id { get; set; }

        public virtual bool IsArchived { get; set; }
    }
}