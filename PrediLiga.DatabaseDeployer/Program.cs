﻿using System;
using System.Collections.Generic;
using System.Threading;
using AcklenAvenue.Data.NHibernate;
using DomainDrivenDatabaseDeployer;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using PrediLiga.Data;

namespace PrediLiga.DatabaseDeployer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var connectionString = ConnectionStrings.Get();

            var databaseConfiguration =
                MsSqlConfiguration.MsSql2008.ShowSql().ConnectionString(x => x.Is(connectionString));

            DomainDrivenDatabaseDeployer.DatabaseDeployer dd = null;
            var sessionFactory = new SessionFactoryBuilder(new MappingScheme(), databaseConfiguration)
                .Build(cfg => { dd = new DomainDrivenDatabaseDeployer.DatabaseDeployer(cfg); });

            Console.WriteLine("");
            Console.WriteLine("Database dropped.");
            dd.Drop();
            Thread.Sleep(1000);

            dd.Create();
            Console.WriteLine("");
            Console.WriteLine("Database created.");

            var session = sessionFactory.OpenSession();
            using (var tx = session.BeginTransaction())
            {
                dd.Seed(new List<IDataSeeder>
                {
                     new LeagueSeeder(session),
                     new AccountSeeder(session),
                     new AccountLeaguesSeeder(session)
                });
                tx.Commit();
            }
            session.Close();
            sessionFactory.Close();
            Console.WriteLine("");
            Console.WriteLine("Seed data added.");
            Thread.Sleep(2000);
        }
    }
}