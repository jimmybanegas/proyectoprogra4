﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDatabaseDeployer;
using NHibernate;
using PrediLiga.Domain.Entities;

namespace PrediLiga.DatabaseDeployer
{
    public class AccountLeaguesSeeder : IDataSeeder
    {
        readonly ISession _session;

        public AccountLeaguesSeeder(ISession session)
        {
            _session = session;
        }

        public void Seed()
        {
            var accountLeague = new AccountLeagues
            {
                User = _session.QueryOver<Account>().Where(x => x.Email == "test@test.com").SingleOrDefault<Account>(),
                League = _session.QueryOver<Leagues>().Where(x => x.NombreLiga == "Liga BBVA").SingleOrDefault<Leagues>()
            };
            _session.Save(accountLeague);

        }
    }
}
