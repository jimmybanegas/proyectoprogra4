﻿using System;
using DomainDrivenDatabaseDeployer;
using NHibernate;
using PrediLiga.Domain.Entities;

namespace PrediLiga.DatabaseDeployer
{
    public class AccountSeeder : IDataSeeder
    {
        readonly ISession _session;

        public AccountSeeder(ISession session)
        {
            _session = session;
        }

        public void Seed()
        {

            var account = new Account
            {
                Email = "test@test.com",
                Nombre = "Test Name",
                Contrasena = "password",
                Alias = "Admin"
            };
            _session.Save(account);
        }
    }
}