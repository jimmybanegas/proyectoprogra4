﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDrivenDatabaseDeployer;
using NHibernate;
using PrediLiga.Domain.Entities;

namespace PrediLiga.DatabaseDeployer
{
    public class LeagueSeeder : IDataSeeder
    {
        readonly ISession _session;

        public LeagueSeeder(ISession session)
        {
            _session = session;
        }

        public void Seed()
        {
            var ligaBbva = new Leagues
            {
                NombreLiga = "Liga BBVA"
            };

            _session.Save(ligaBbva);

            var ligaPremier = new Leagues
            {
                NombreLiga = "Liga Premier Inglesa"
            };

            _session.Save(ligaPremier);

            var ligaItaliana = new Leagues
            {
                NombreLiga = "Seria A"
            };

            _session.Save(ligaItaliana);
        }
    }
}
