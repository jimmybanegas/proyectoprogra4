namespace PrediLiga.Api
{
    public interface IBootstrapperTask
    {
        void Run();
    }
}