using AutoMapper;
using PrediLiga.Api.Models;
using PrediLiga.Domain.Entities;

namespace PrediLiga.Api
{
    public class ConfigureAutomapper : IBootstrapperTask
    {
        #region IBootstrapperTask Members

        public void Run()
        {
            //automappings go here
            //Ex: Mapper.CreateMap<SomeType, SomeOtherType>().ReverseMap();
            Mapper.CreateMap<AccountRegisterModel, Account>().ReverseMap();
            Mapper.CreateMap<AddLeagueModel, Leagues>().ReverseMap();
            Mapper.CreateMap<AddTeamModel, Team>().ReverseMap();
            Mapper.CreateMap<AddMatchModel, Match>().ReverseMap();
            Mapper.CreateMap<AddPredictionModel, Prediction>().ReverseMap();
            Mapper.CreateMap<AddSuscriptionModel, AccountLeagues>().ReverseMap();

            Mapper.CreateMap<Leagues, LeaguesModel>().ReverseMap();
            Mapper.CreateMap<Team, TeamsModel>().ReverseMap();
            Mapper.CreateMap<Match, MatchesModel>().ReverseMap();
            Mapper.CreateMap<Prediction, PredictionsModel>().ReverseMap();
            Mapper.CreateMap<Prediction, PredictionsModelResponse>().ReverseMap();
            Mapper.CreateMap<AccountLeagues, SuscriptionModel>().ReverseMap();
            Mapper.CreateMap<Account, SuscriptorsModel>().ReverseMap();
            Mapper.CreateMap<Account, PositionsModel>().ReverseMap();
        }

        #endregion
    }
}