﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using AutoMapper;
using PrediLiga.Api.Models;
using PrediLiga.Domain.Entities;
using PrediLiga.Domain.Services;

namespace PrediLiga.Api.Controllers
{
    public class SuscriptionController : BaseApiController
    {
        readonly IReadOnlyRepository _readOnlyRepository;
        readonly IMappingEngine _mappingEngine;
        readonly IWriteOnlyRepository _writeOnlyRepository;


        public SuscriptionController(IReadOnlyRepository readOnlyRepository, IMappingEngine mappingEngine, IWriteOnlyRepository writeOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
            _mappingEngine = mappingEngine;
            _writeOnlyRepository = writeOnlyRepository;
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("addSuscription/{Id}")]
        public AddSuscriptionResponseModel AddSuscription(long Id,AuthModel res)
        {
         
            var account =
               _readOnlyRepository.First<Account>(
                   account1 => account1.Email == res.email);

            var liga =
              _readOnlyRepository.GetById<Leagues>(Id);


            var suscription = new AccountLeagues()
            {
                User = account,
                League = liga
            };

            var suscriptionCreated = _writeOnlyRepository.Create(suscription);

            if (suscriptionCreated == null) 
                return new AddSuscriptionResponseModel()
                {
                    Message = "Hubo un error al suscribirse",
                    Status = 0
                };

            return new AddSuscriptionResponseModel()
            {
                Message = "Te has suscrito!",
                Status = 1
            };
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("deleteSuscription/{Id}")]
        public AddSuscriptionResponseModel ArchiveSuscription(long Id)
        {
            var suscriptionExist = _readOnlyRepository.GetById<AccountLeagues>(Id);

            _writeOnlyRepository.Delete<AccountLeagues>(suscriptionExist.Id);

            return new AddSuscriptionResponseModel()
            {
                Message = "Suscripción borrada exitosamente",
                Status = 1
            };
        }

        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("suscriptions/available")]
        public List<SuscriptionModel> GetAvailableSuscriptions()
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

            var suscriptions = _readOnlyRepository.GetAll<AccountLeagues>().ToList();
            var suscriptionModel = _mappingEngine.Map<List<AccountLeagues>, List<SuscriptionModel>>(suscriptions);
            return suscriptionModel;
        }

        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("suscriptors/{Id}")]
        public List<SuscriptorsModel> GetSuscriptors(long Id)
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");
            
            //    Acceder a los usuarios que tiene una liga
            var usuarios = _readOnlyRepository.Query<AccountLeagues>(x => x.League.Id == Id).Select(y => y.User).ToList();

            var suscriptionModel = _mappingEngine.Map<List<Account>, List<SuscriptorsModel>>(usuarios);

            return suscriptionModel;
        }

        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("positions")]
        public List<PositionsModel> GetPositions()
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

         
            var users = _readOnlyRepository.GetAll<Account>();
            foreach (var user in users)
            {
                user.PartidosAcertados = 0;
                user.Puntaje = 0;
                _writeOnlyRepository.Update(user);
            }

           //    Acceder a todas las predicciones de todas las ligas
            
           var predicciones = _readOnlyRepository.GetAll<Prediction>().ToList();
           foreach (var prediccion in predicciones)
            {
                if (prediccion.Equipo1 == prediccion.Match.ResultadoEquipo1 &&
                    prediccion.Equipo2 == prediccion.Match.ResultadoEquipo2 && prediccion.Match.IsArchived)
                {
                    prediccion.Usuario.PartidosAcertados += 1;
                    prediccion.Usuario.Puntaje += 5;

                    _writeOnlyRepository.Update(prediccion.Usuario);
                }
            }

            var usuarios = _readOnlyRepository.GetAll<Account>().OrderByDescending(user => user.Puntaje).ToList();

            var positionsModel = _mappingEngine.Map<List<Account>, List<PositionsModel>>(usuarios).ToList();

            return positionsModel;
        }
    }
}