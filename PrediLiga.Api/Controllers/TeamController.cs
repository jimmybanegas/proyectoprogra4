﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using AutoMapper;
using PrediLiga.Api.Models;
using PrediLiga.Domain.Entities;
using PrediLiga.Domain.Services;

namespace PrediLiga.Api.Controllers
{
    public class TeamController : BaseApiController
    {
        readonly IReadOnlyRepository _readOnlyRepository;
        readonly IMappingEngine _mappingEngine;
        readonly IWriteOnlyRepository _writeOnlyRepository;


        public TeamController(IReadOnlyRepository readOnlyRepository, IMappingEngine mappingEngine, IWriteOnlyRepository writeOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
            _mappingEngine = mappingEngine;
            _writeOnlyRepository = writeOnlyRepository;
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("addTeam")]
        public AddTeamResponseModel AddTeam([FromBody] AddTeamModel model)
        {
           var teamExist = _readOnlyRepository.First<Team>(
                account1 => account1.Nombre == model.Nombre);

            if (teamExist != null) return new AddTeamResponseModel()
            {
                Message = "Ya existe un equipo con ese nombre",
                Status = 0
            };

            var team = _mappingEngine.Map<AddTeamModel, Team>(model);

            var teamCreated = _writeOnlyRepository.Create(team);

            if (teamCreated == null) 
                return new AddTeamResponseModel()
                {
                    Message = "Hubo un error al guardar el equipo",
                    Status = 0
                };

            return new AddTeamResponseModel()
            {
                Message = "Equipo guardado exitosamente",
                Status = 1
            };
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("editTeam")]
        public AddTeamResponseModel EditTeam([FromBody] TeamsModel model)
        {
            var teamExist = _readOnlyRepository.GetById<Team>(model.Id);

            teamExist.Id = model.Id;
            teamExist.IsArchived = model.IsArchived;
            teamExist.Logo = model.Logo;
            teamExist.Nombre = model.Nombre;
            teamExist.SitioWeb = model.SitioWeb;
           
            var teamUpdated = _writeOnlyRepository.Update(teamExist);

            if (teamUpdated == null)
                return new AddTeamResponseModel()
                {
                    Message = "Hubo un error al actualizar el equipo",
                    Status = 0
                };

            return new AddTeamResponseModel()
            {
                Message = "Equipo actualizado exitosamente",
                Status = 1
            };
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("deleteTeam/{Id}")]
        public AddTeamResponseModel ArchiveTeam(long Id)
        {
           var teamExist = _readOnlyRepository.GetById<Team>(Id);

           _writeOnlyRepository.Delete<Team>(teamExist.Id);

            return new AddTeamResponseModel()
            {
                Message = "Equipo borrado exitosamente",
                Status = 1
            };
        }


        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("teams/available")]
        public List<TeamsModel> GetAvailableTeams()
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

            var teams = _readOnlyRepository.GetAll<Team>().ToList();
            var teamsModel = _mappingEngine.Map<List<Team>, List<TeamsModel>>(teams);
            return teamsModel;
        }
    }
}