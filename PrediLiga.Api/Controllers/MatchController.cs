﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AttributeRouting.Helpers;
using AttributeRouting.Web.Mvc;
using AutoMapper;
using PrediLiga.Api.Models;
using PrediLiga.Domain.Entities;
using PrediLiga.Domain.Services;

namespace PrediLiga.Api.Controllers
{
    public class MatchController : BaseApiController
    {
        readonly IReadOnlyRepository _readOnlyRepository;
        readonly IMappingEngine _mappingEngine;
        readonly IWriteOnlyRepository _writeOnlyRepository;
        
        public MatchController(IReadOnlyRepository readOnlyRepository, IMappingEngine mappingEngine, IWriteOnlyRepository writeOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
            _mappingEngine = mappingEngine;
            _writeOnlyRepository = writeOnlyRepository;
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("addMatch")]
        public AddMatchResponseModel AddMatch([FromBody] AddMatchModel model)
        {
          /* var matchExist = _readOnlyRepository.First<Match>(
                account1 => account1.Team1 == model.Team1);

            if (matchExist != null) return new AddMatchResponseModel()
            {
                Message = "Ya existe un partido fijado",
                Status = 0
            };
            */
            var match = _mappingEngine.Map<AddMatchModel, Match>(model);

            var matchCreated = _writeOnlyRepository.Create(match);

            if (matchCreated == null) 
                return new AddMatchResponseModel()
                {
                    Message = "Hubo un error al guardar el partido",
                    Status = 0
                };

            return new AddMatchResponseModel()
            {
                Message = "Patido guardado exitosamente",
                Status = 1
            };
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("editMatch")]
        public AddMatchResponseModel EditMatch([FromBody] MatchesModel model)
        {
            var matchExist = _readOnlyRepository.GetById<Match>(model.Id);

            if (model.Id == 0)
            {
                return new AddMatchResponseModel()
                {
                    Message = "Hubo un error al actualizar el partido",
                    Status = 3
                };
            }
         //   matchExist.Id = model.Id;
            matchExist.IsArchived = model.IsArchived;
            matchExist.Lugar = model.Lugar;
            if (!matchExist.IsArchived)
            {
                matchExist.ResultadoEquipo1 = model.ResultadoEquipo1;
                matchExist.ResultadoEquipo2 = model.ResultadoEquipo2;   
            }
          //  matchExist.League = model.League;
            if (model.Team1 != 0)
                 matchExist.Team1 = model.Team1;

            if (model.Team2 != 0)
                matchExist.Team2 = model.Team2;
           
            matchExist.HorayFecha = model.HorayFecha;

            var matchUpdated = _writeOnlyRepository.Update(matchExist);

            if (matchUpdated == null)
                return new AddMatchResponseModel()
                {
                    Message = "Hubo un error al actualizar el partido",
                    Status = 0
                };

            return new AddMatchResponseModel()
            {
                Message = "Partido actualizado exitosamente",
                Status = 1
            };
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("deleteMatch/{Id}")]
        public AddMatchResponseModel ArchiveMatch(long Id)
        {
            var matchExist = _readOnlyRepository.GetById<Match>(Id);

            _writeOnlyRepository.Delete<Match>(matchExist.Id);

            return new AddMatchResponseModel()
            {
                Message = "Partido borrado exitosamente",
                Status = 1
            };
        }


        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("matches/available")]
        public List<MatchesModel> GetAvailableMatches()
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

            var matches = _readOnlyRepository.GetAll<Match>().ToList();
            var matchesModel = _mappingEngine.Map<List<Match>, List<MatchesModel>>(matches);

            return matchesModel;
        }

        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("matches/suscribed")]
        public List<MatchesModel> GetSuscribedMatches()
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

          //    Acceder a las ligas a las que está suscrito el usuario
            var account = _readOnlyRepository.Query<AccountLeagues>(x => x.User.Email == userTokenModel.Email).Select(y => y.League);
        
            var matchesModel = new List<MatchesModel>();

            foreach (var liga in account)
            {
                var matches = _readOnlyRepository.GetAll<Match>();
           
                matchesModel.AddRange(from match in matches
                    where liga.Id == match.League
                    select new MatchesModel()
                    {
                        Id = match.Id, IsArchived = match.IsArchived,
                        League = match.League, Team1 = match.Team1, 
                        Team2 = match.Team2, HorayFecha = match.HorayFecha,
                        Lugar = match.Lugar,
                        ResultadoEquipo1 = match.ResultadoEquipo1,
                        ResultadoEquipo2 = match.ResultadoEquipo2
                    });
            }

            return matchesModel;
        }
    }
}