﻿using System;
using System.Linq;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using AutoMapper;
using PrediLiga.Api.Controllers.AccountControllerHelpers;
using PrediLiga.Api.Models;
using PrediLiga.Domain.Entities;
using PrediLiga.Domain.Services;

namespace PrediLiga.Api.Controllers
{
    public class AccountController : BaseApiController
    {
        readonly IMappingEngine _mappingEngine;
        readonly IReadOnlyRepository _readOnlyRepository;
        readonly IRegisterValidator<AccountRegisterModel> _registerValidator;
        readonly IRegisterValidator<ChangePassModel> _restoreValidator;
        readonly IWriteOnlyRepository _writeOnlyRepository;

        public AccountController(IReadOnlyRepository readOnlyRepository, IWriteOnlyRepository writeOnlyRepository, IMappingEngine mappingEngine)
        {
            _readOnlyRepository = readOnlyRepository;
            _writeOnlyRepository = writeOnlyRepository;
            _mappingEngine = mappingEngine;
        }

        public AccountController(IReadOnlyRepository readOnlyRepository, IWriteOnlyRepository writeOnlyRepository,
            IMappingEngine mappingEngine, IRegisterValidator<AccountRegisterModel> registerValidator,
            IRegisterValidator<ChangePassModel> restoresValidator)
        {
            _readOnlyRepository = readOnlyRepository;
            _writeOnlyRepository = writeOnlyRepository;
            _mappingEngine = mappingEngine;
            _registerValidator = registerValidator;
            _restoreValidator = restoresValidator;
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("login")]
        public AuthModel Login([FromBody] AccountLoginModel model)
        {
            var encryptObj = new EncryptServices();

            var account =
                _readOnlyRepository.First<Account>(
                    account1 => account1.Email == model.Email);

            if (account == null)
                return new AuthModel()
                {
                    Status = 0,
                    access_token = "Lo sentimos, No existe un usuario con ese Email"
                };

            if (account.Contrasena !=
                encryptObj.EncryptStringToBytes(model.Contrasena, account.EncryptKey, account.EncryptIv))
            {
                return new AuthModel()
                {
                   Status = 0,
                   access_token = "Clave incorrecta"
                };
            }
           
            var authModel = new AuthModel
            {
                email = account.Email,
                access_token = AuthRequestFactory.BuildEncryptedRequest(account.Email),
                Status = 1,
                role = new RoleModel
                {
                    bitMask = 2,
                    title = account.Alias
                }
            };
            return authModel;
        }

        [POST("register")]
        public AccountRegisterResponseModel Register([FromBody] AccountRegisterModel model)
        {
            var accountExist = _readOnlyRepository.First<Account>(
                account1 => account1.Email == model.Email);

            if (accountExist != null) return new AccountRegisterResponseModel(model.Email, model.Nombre, 0);
            var account = _mappingEngine.Map<AccountRegisterModel, Account>(model);

            var encryptObj = new EncryptServices();
            encryptObj.GenerateKeys();
            account.Contrasena = encryptObj.EncryptStringToBytes(account.Contrasena, encryptObj.MyRijndael.Key,
                encryptObj.MyRijndael.IV);
            account.EncryptKey = encryptObj.MyRijndael.Key;
            account.EncryptIv = encryptObj.MyRijndael.IV;

            account.Alias = "user";

            var accountCreated = _writeOnlyRepository.Create(account);

            if (accountCreated == null)
                return new AccountRegisterResponseModel
                {
                    Message = "Hubo un error al guardar el usuario",
                    Status = 0
                };
           MailgunServices.SendSingUpMessage(accountCreated.Nombre, accountCreated.Apellido, accountCreated.Email,
                accountCreated.Contrasena);
//
            return new AccountRegisterResponseModel(accountCreated.Email, accountCreated.Nombre, 2);
        }

        [POST("resetpassword")]
        public ResetPasswordResponseModel ResetPassword([FromBody] ChangePassModel model)
        {
            var account =
                _readOnlyRepository.First<Account>(
                    account1 => account1.Email == model.Email);

            if (account == null)
                return new ResetPasswordResponseModel
                {
                    Message = "No existe ningun usuario registrado en SportLiga con ese correo",
                    Status = 1
                };
            var encryptObj = new EncryptServices();
            encryptObj.GenerateKeys();
            var newPassword = Guid.NewGuid().ToString();
            account.Contrasena = encryptObj.EncryptStringToBytes(newPassword, encryptObj.MyRijndael.Key,
                encryptObj.MyRijndael.IV);
            account.EncryptKey = encryptObj.MyRijndael.Key;
            account.EncryptIv = encryptObj.MyRijndael.IV;

           _writeOnlyRepository.Update(account);

            MailgunServices.SendRestoreMessage(account.Nombre, account.Apellido, account.Email, newPassword);
            return new ResetPasswordResponseModel
            {
                Message = "Se acaba de enviar un mensaje al correo indicado, favor seguir las instrucciones",
                Status = 2
            };
        }


    }

}