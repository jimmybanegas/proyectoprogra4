﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using AutoMapper;
using AutoMapper.Internal;
using PrediLiga.Api.Models;
using PrediLiga.Domain.Entities;
using PrediLiga.Domain.Services;

namespace PrediLiga.Api.Controllers
{
    public class PredictionController : BaseApiController
    {
        readonly IReadOnlyRepository _readOnlyRepository;
        readonly IMappingEngine _mappingEngine;
        readonly IWriteOnlyRepository _writeOnlyRepository;


        public PredictionController(IReadOnlyRepository readOnlyRepository, IMappingEngine mappingEngine, IWriteOnlyRepository writeOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
            _mappingEngine = mappingEngine;
            _writeOnlyRepository = writeOnlyRepository;
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("addPrediction")]
        public AddPredictionResponseModel AddPrediction([FromBody] AddPredictionModel model)
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

            var account =
            _readOnlyRepository.First<Account>(
                account1 => account1.Email == userTokenModel.Email);

            var match =
           _readOnlyRepository.GetById<Match>(model.Match);

          
            if (account == null || match == null)
                return new AddPredictionResponseModel()
                {
                    Message = "Hubo un error al guardar la prediccion",
                    Status = 0
                };
            
            //Editando uno ya existente
            var predExist = _readOnlyRepository.First<Prediction>(
                account1 => account1.Usuario == account && account1.Match.Id == model.Match);

            if (predExist != null)
            {
                predExist.Equipo1 = model.Equipo1;
                predExist.Equipo2 = model.Equipo2;

                var predUpdated = _writeOnlyRepository.Update(predExist);

                if (predUpdated == null)
                    return new AddPredictionResponseModel()
                    {
                        Message = "Hubo un error al actualizar la predicción",
                        Status = 0
                    };

                return new AddPredictionResponseModel()
                {
                    Message = "Predicción actualizada",
                    Status = 1
                };
            }

            var pred= new PredictionsModel()
            {
               Usuario = account,
               Match = match,
               Equipo1 = model.Equipo1,
               Equipo2 = model.Equipo2
            };

            var prediction = _mappingEngine.Map<PredictionsModel, Prediction>(pred);

            var predictionCreated = _writeOnlyRepository.Create(prediction);

            if (predictionCreated == null) 
                return new AddPredictionResponseModel()
                {
                    Message = "Hubo un error al guardar la prediccion",
                    Status = 0
                };

            return new AddPredictionResponseModel()
            {
                Message = "Predicción guardada exitosamente",
                Status = 1
            };
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("editPrediction")]
        public AddPredictionResponseModel EditPrediction([FromBody] PredictionsModel model)
        {
            var predictionExist = _readOnlyRepository.GetById<Prediction>(model.Id);

            predictionExist.Id = model.Id;
            predictionExist.IsArchived = model.IsArchived;
            predictionExist.Usuario = model.Usuario;
            predictionExist.Match = model.Match;
            predictionExist.Equipo1 = model.Equipo1;
            predictionExist.Equipo2 = model.Equipo2;

            var predictionUpdated = _writeOnlyRepository.Update(predictionExist);

            if (predictionUpdated == null)
                return new AddPredictionResponseModel()
                {
                    Message = "Hubo un error al actualizar su predicción",
                    Status = 0
                };

            return new AddPredictionResponseModel()
            {
                Message = "Prediction actualizado exitosamente",
                Status = 1
            };
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("deletePrediction/{Id}")]
        public AddPredictionResponseModel ArchivePrediction(long Id)
        {
            var predictionExist = _readOnlyRepository.GetById<Prediction>(Id);

            _writeOnlyRepository.Delete<Prediction>(predictionExist.Id);

            return new AddPredictionResponseModel()
            {
                Message = "Predicción borrada exitosamente",
                Status = 1
            };
        }

        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("predictions/available")]
        public List<PredictionsModelResponse> GetAvailablePredictions()
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

           // var predictions = _readOnlyRepository.GetAll<Prediction>().ToList();
            var predictions = _readOnlyRepository.Query<Prediction>(x => x.Usuario.Email == userTokenModel.Email).ToList();

            var predictionModel = _mappingEngine.Map<List<Prediction>, List<PredictionsModel>>(predictions);
            
            return predictionModel.Select(prediccion => new PredictionsModelResponse()
            {
                Id = prediccion.Id, IsArchived = prediccion.IsArchived, 
                Equipo1 = prediccion.Equipo1, Equipo2 = prediccion.Equipo2,
                Partido = prediccion.Match.Id
            }).ToList();
        }

    }
}