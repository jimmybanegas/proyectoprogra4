using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using AutoMapper;
using NHibernate.Impl;
using PrediLiga.Api.Models;
using PrediLiga.Domain.Entities;
using PrediLiga.Domain.Services;

namespace PrediLiga.Api.Controllers
{
    public class LeaguesController : BaseApiController
    {
        readonly IReadOnlyRepository _readOnlyRepository;
        readonly IMappingEngine _mappingEngine;
        readonly IWriteOnlyRepository _writeOnlyRepository;


        public LeaguesController(IReadOnlyRepository readOnlyRepository, IMappingEngine mappingEngine, IWriteOnlyRepository writeOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
            _mappingEngine = mappingEngine;
            _writeOnlyRepository = writeOnlyRepository;
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("addleague")]
        public AddLeagueResponseModel AddLeague([FromBody] AddLeagueModel model)
        {
           var leagueExist = _readOnlyRepository.First<Leagues>(
                account1 => account1.NombreLiga == model.NombreLiga);

            if (leagueExist != null) return new AddLeagueResponseModel()
            {
                Message = "Ya existe una liga con ese nombre",
                Status = 0
            };

            var league = _mappingEngine.Map<AddLeagueModel, Leagues>(model);

            var leagueCreated = _writeOnlyRepository.Create(league);

            if (leagueCreated == null) 
                return new AddLeagueResponseModel()
                {
                    Message = "Hubo un error al guardar la liga",
                    Status = 0
                };

            return new AddLeagueResponseModel()
            {
                Message = "Liga guardada exitosamente",
                Status = 1
            };
        }


        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("editLeague")]
        public AddLeagueResponseModel EditLeague([FromBody] LeaguesModel model)
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

            var leagueExist = _readOnlyRepository.GetById<Leagues>(model.Id);

            leagueExist.Id = model.Id;
            leagueExist.IsArchived = model.IsArchived;
            leagueExist.Logo = model.Logo;
            leagueExist.NombreLiga = model.NombreLiga;
            leagueExist.SitioWeb = model.SitioWeb;

            var leagueUpdated = _writeOnlyRepository.Update(leagueExist);

            if (leagueUpdated == null)
                return new AddLeagueResponseModel()
                {
                    Message = "Hubo un error al actualizar la liga",
                    Status = 0
                };

            return new AddLeagueResponseModel()
            {
                Message = "Liga actualizada exitosamente",
                Status = 1
            };
        }

        [HttpPost]
        [AcceptVerbs("POST", "HEAD")]
        [POST("deleteLeague/{Id}")]
        public AddLeagueResponseModel ArchiveLeague(long Id)
        {
            /*var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");*/

            var leagueExist = _readOnlyRepository.GetById<Leagues>(Id);

            _writeOnlyRepository.Delete<Leagues>(leagueExist.Id);

            return new AddLeagueResponseModel()
            {
                Message = "Liga borrada exitosamente",
                Status = 1
            };
        }

        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("league/{Id}")]
        public LeaguesModel GetLeagueById(long Id)
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

            var leagueExist = _readOnlyRepository.GetById<Leagues>(Id);

            var leaguesModel =  _mappingEngine.Map<Leagues, LeaguesModel>(leagueExist);
            ;
            return leaguesModel;
        }

        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("leagues/available")]
        public List<LeaguesModel> GetAvailableLeagues()
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

            var account = _readOnlyRepository.Query<AccountLeagues>(x => x.User.Email == userTokenModel.Email).Select(y => y.League);
            var leagues = _readOnlyRepository.GetAll<Leagues>().Except(account).ToList();
            var leaguesModel = _mappingEngine.Map<List<Leagues>, List<LeaguesModel>>(leagues);
            return leaguesModel;
        }

        [HttpGet]
        [AcceptVerbs("GET", "HEAD")]
        [GET("leagues/suscribed")]
        public List<LeaguesModel> GetSuscribedLeagues()
        {
            var userTokenModel = GetUserTokenModel();
            if (userTokenModel == null)
                throw new HttpException((int)HttpStatusCode.Unauthorized, "User is not authorized");

            var account = _readOnlyRepository.Query<AccountLeagues>(x => x.User.Email == userTokenModel.Email).Select(y => y.League);
            var leaguesModel = _mappingEngine.Map<List<Leagues>, List<LeaguesModel>>(account.ToList());
            return leaguesModel;
        }
    }
}