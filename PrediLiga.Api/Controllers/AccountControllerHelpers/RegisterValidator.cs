using System.Text.RegularExpressions;
using PrediLiga.Api.Models;
using PrediLiga.Domain.Services;

namespace PrediLiga.Api.Controllers.AccountControllerHelpers
{
    public class RegisterValidator : IRegisterValidator<AccountRegisterModel>
    {
        public string Validate(AccountRegisterModel model)
        {
            if (IsValidEmail(model.Email))
                return "Formato de Correo Incorrecto";
            return model.Contrasena != model.ConfirmarContrasena ? "Claves no son iguales" : "";
        }

        public static bool IsValidEmail(string strMailAddress)
        {
            // Return true if strMailAddress is invalid e-mail format.
            return Regex.IsMatch(strMailAddress,
                @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))
            |[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))"
                + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)
            +[a-zA-Z]{2,6}))$");
        }
    }
}