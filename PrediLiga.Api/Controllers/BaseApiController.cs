﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Web.Http;
using PrediLiga.Api.Models;

namespace PrediLiga.Api.Controllers
{
    public class BaseApiController :ApiController
    {
        protected UserTokenModel GetUserTokenModel()
        {
            IEnumerable<string> headerValues = Request.Headers.GetValues("Authorization");
            var authorizationToken = headerValues.FirstOrDefault();
            var userTokenModel = AuthRequestFactory.BuildDecryptedRequest(authorizationToken);
            return userTokenModel;
        }
    }
}