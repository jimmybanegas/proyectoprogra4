﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrediLiga.Api.Models
{
    public class TeamsModel
    {
        public long Id { get; set; }
        public long Liga { get; set; }
        public string Nombre { get; set; }
        public string SitioWeb { get; set; }
        public string Logo { get; set; }
        public bool IsArchived { get; set; }
    }
}