﻿namespace PrediLiga.Api.Models
{
    public class AddMatchModel
    {
        public long League { get; set; }
        public long Team1 { get; set; }
        public long Team2 { get; set; }
        public string HorayFecha { get; set; }
        public string Lugar { get; set; }
        public bool IsArchived { get; set; }
    }
}