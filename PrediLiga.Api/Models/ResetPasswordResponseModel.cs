﻿namespace PrediLiga.Api.Models
{
    public class ResetPasswordResponseModel
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}