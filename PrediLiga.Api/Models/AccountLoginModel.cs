namespace PrediLiga.Api.Models
{
    public class AccountLoginModel
    {
        public string Email { get; set; }
        public string Contrasena { get; set; }
    }
}