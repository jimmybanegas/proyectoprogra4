﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrediLiga.Api.Models
{
    public class LeaguesModel
    {
        public long Id { get; set; }
        public string NombreLiga { get; set; }
        public string PaisLiga { get; set; }
        public string SitioWeb { get; set; }
        public string Logo { get; set; }
        public string CantEquipos { get; set; }
        public bool IsArchived { get; set; }
    }
}