﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrediLiga.Api.Models
{
    public class RoleModel
    {
        public int bitMask { get; set; }
        public string title { get; set; }
    }
}