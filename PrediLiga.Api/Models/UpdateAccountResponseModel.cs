﻿namespace PrediLiga.Api.Models
{
    public class UpdateAccountResponseModel
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}