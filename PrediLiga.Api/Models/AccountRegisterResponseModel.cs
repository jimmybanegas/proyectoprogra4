﻿namespace PrediLiga.Api.Models
{
    public class AccountRegisterResponseModel
    {
        public AccountRegisterResponseModel(string email, string nombre, int caseNumber)
        {
            FirstName = nombre;
            switch (caseNumber)
            {
                case 2:
                    Message = ("Tu cuenta ha sido registrada exitosamente bajo el correo: " + email);
                    Status = 2;
                    break;
                case 0:
                    Message = (nombre + "! Lo sentimos mucho ya existe un usuario registrado con el correo " + email);
                    Status = 0;
                    break;
            }
        }

        public AccountRegisterResponseModel()
        {
        }

        public string FirstName { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
    }
}