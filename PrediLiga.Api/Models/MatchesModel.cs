﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrediLiga.Api.Models
{
    public class MatchesModel
    {
        public long Id { get; set; }
        public bool IsArchived { get; set; }
        public long League { get; set; }
        public long Team1 { get; set; }
        public long Team2 { get; set; }
        public string HorayFecha { get; set; }
        public string Lugar { get; set; }
        public int ResultadoEquipo1 { get; set; }
        public int ResultadoEquipo2 { get; set; }
    }
}