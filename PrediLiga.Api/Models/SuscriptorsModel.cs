﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrediLiga.Api.Models
{
    public class SuscriptorsModel
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
    }
}