﻿using PrediLiga.Domain.Entities;

namespace PrediLiga.Api.Models
{
    public class AddPredictionModel
    {
        public Account Usuario { get; set; }

        public int Equipo1 { get; set; }

        public int Equipo2 { get; set; }

        public long Id { get; set; }

        public bool IsArchived { get; set; }

        public  long Match { get; set; }
    }
}