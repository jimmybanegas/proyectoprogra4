﻿namespace PrediLiga.Api.Models
{
    public class AddTeamResponseModel
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}