﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrediLiga.Api.Models
{
    public class UserTokenModel
    {
        public string Email { get; set; }
    }
}