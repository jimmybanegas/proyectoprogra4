﻿namespace PrediLiga.Api.Models
{
    public class AddMatchResponseModel
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}