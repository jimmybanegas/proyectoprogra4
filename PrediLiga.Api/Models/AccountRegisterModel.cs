namespace PrediLiga.Api.Models
{
    public class AccountRegisterModel
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Alias { get; set; }
        public string Email { get; set; }
        public string Contrasena { get; set; }
        public string ConfirmarContrasena { get; set; }
    }
}