﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrediLiga.Domain.Entities;

namespace PrediLiga.Api.Models
{
    public class PredictionsModel
    {
        public long Id { get; set; }

        public bool IsArchived { get; set; }

        public Account Usuario { get; set; }

        public int Equipo1 { get; set; } //Resultado que usuario predice para el equipo 1

        public int Equipo2 { get; set; } //Resultado que usuario predice para el equipo 2

        public Match Match { get; set; }  //Id del partido al que está asociado la predicción
    }
}