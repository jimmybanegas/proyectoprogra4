﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrediLiga.Domain.Entities;

namespace PrediLiga.Api.Models
{
    public class PredictionsModelResponse
    {
        public long Id { get; set; }

        public bool IsArchived { get; set; }

        public int Equipo1 { get; set; } //Resultado que usuario predice para el equipo 1

        public int Equipo2 { get; set; } //Resultado que usuario predice para el equipo 2

        public long Partido { get; set; }
    }
}