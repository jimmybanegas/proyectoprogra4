﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrediLiga.Api.Models
{
    public class PositionsModel
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Puntaje { get; set; }   
        public string PartidosAcertados { get; set; }   
    }
}