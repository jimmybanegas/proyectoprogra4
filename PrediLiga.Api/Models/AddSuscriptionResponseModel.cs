﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrediLiga.Api.Models
{
    public class AddSuscriptionResponseModel
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}