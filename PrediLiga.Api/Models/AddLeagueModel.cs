﻿namespace PrediLiga.Api.Models
{
    public class AddLeagueModel
    {
        public string NombreLiga { get; set; }
        public string PaisLiga { get; set; }
        public string SitioWeb { get; set; }
        public string Logo { get; set; }
        public string CantEquipos { get; set; }
        public string IsArchived { get; set; }
    }
}