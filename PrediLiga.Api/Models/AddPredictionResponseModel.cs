﻿namespace PrediLiga.Api.Models
{
    public class AddPredictionResponseModel
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}