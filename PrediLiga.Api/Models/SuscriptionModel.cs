﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrediLiga.Domain.Entities;

namespace PrediLiga.Api.Models
{
    public class SuscriptionModel
    {
        public long Id { get; set; }

        public long User { get; set; }

        public long League { get; set; }

        public bool IsArchived { get; set; }
    }
}