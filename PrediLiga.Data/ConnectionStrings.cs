using System.Configuration;

namespace PrediLiga.Data
{
    public class ConnectionStrings
    {
        public static string Get()
        {
            var local = ConfigurationManager.ConnectionStrings["local"].ToString();

            //var remote = (ConfigurationManager.ConnectionStrings["remote"].ToString());

            //var production = (ConfigurationManager.ConnectionStrings["production"].ToString());

            var environment = (ConfigurationManager.AppSettings["Environment"] ?? "").ToLower();
            var connectionStringToUse = local;


            switch (environment)
            {
                case "remote":
                case "qa":
                    connectionStringToUse = ConfigurationManager.ConnectionStrings["remote"].ToString();
                    break;
                case "production":
                    connectionStringToUse = string.Empty;
                    break;
            }

            return connectionStringToUse;
        }
    }
}