﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using PrediLiga.Domain.Entities;
using PrediLiga.Domain.Services;

namespace PrediLiga.Data
{
    public class WriteOnlyRepository : IWriteOnlyRepository
    {
        private readonly ISession _session;

        public WriteOnlyRepository(ISession session)
        {
            _session = session;
        }

        public T Create<T>(T itemToCreate) where T : IEntity
        {
            _session.Save(itemToCreate);
            return itemToCreate;
        }

        public void DeleteAll<T>() where T : class, IEntity
        {
            foreach (T item in _session.QueryOver<T>().List())
            {
                Delete<T>(item.Id);
            }
        }

        public IEnumerable<T> CreateAll<T>(IEnumerable<T> list) where T : IEntity
        {
            List<T> items = list as List<T> ?? list.ToList();
            foreach (T item in items)
            {
                Create(item);
            }

            return items;
        }

        public void Delete<T>(long itemId) where T : IEntity
        {
            var itemToDelete = _session.Get<T>(itemId);
            _session.Delete(itemToDelete);
        }

        public T Update<T>(T itemToUpdate) where T : IEntity
        {
            ISession session = _session;
            session.Update(itemToUpdate);
            return itemToUpdate;
        }

        public T Archive<T>(T itemToArchive) where T : class,IEntity
        {
            itemToArchive.IsArchived = true;
            return Update(itemToArchive);
        }
    }
}