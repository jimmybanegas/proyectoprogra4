﻿using System.Collections.Generic;

namespace PrediLiga.Domain.Entities
{
    public class Match : IEntity
    {
        public virtual long Id { get; set; }

        public virtual bool IsArchived { get; set; }
        
        public virtual long League { get; set; }

        public virtual long Team1 { get; set; }

        public virtual long Team2 { get; set; }

        public virtual string HorayFecha { get; set; }

        public virtual string Lugar { get; set; }

        public virtual int ResultadoEquipo1 { get; set; }

        public virtual int ResultadoEquipo2 { get; set; }

    }
}