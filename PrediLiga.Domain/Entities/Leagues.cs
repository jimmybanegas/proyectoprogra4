﻿using System.Collections.Generic;

namespace PrediLiga.Domain.Entities
{
    public class Leagues : IEntity
    {
        public virtual long Id { get; set; }

        public virtual bool IsArchived { get; set; }

        public virtual string NombreLiga { get; set; }

        public virtual string PaisLiga { get; set; }

        public virtual string CantEquipos { get; set; }

        public virtual string SitioWeb { get; set; }

        public virtual string Logo { get; set; }

    }
}