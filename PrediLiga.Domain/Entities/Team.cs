﻿namespace PrediLiga.Domain.Entities
{
    public class Team : IEntity
    {
        public virtual string Nombre { get; set; }

        public virtual string SitioWeb { get; set; }

        public virtual string Logo { get; set; }

        public virtual long Id { get; set; }

        public virtual bool IsArchived { get; set; }

        public virtual long Liga { get; set; }
        
    }
}