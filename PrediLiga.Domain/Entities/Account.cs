using System.Collections.Generic;
using System.Linq;

namespace PrediLiga.Domain.Entities
{
    public class Account : IEntity
    {
        public virtual long Id { get; set; }

        public virtual bool IsArchived { get; set; }

        public virtual string Email { get; set; }

        public virtual string Nombre { get; set; }

        public virtual string Alias { get; set; }

        public virtual string Apellido { get; set; }

        public virtual string Contrasena { get; set; }

        public virtual byte[] EncryptKey { get; set; }

        public virtual byte[] EncryptIv { get; set; }

        public virtual int Puntaje  { get; set; }

        public virtual int PartidosAcertados { get; set; }

        public virtual bool CheckPassword(string password)
        {
            return true;
        }

        
    }
}